import Vue from "vue";
import App from "./App.vue";
import router from './router';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import vuetify from './plugins/vuetify';
import firebase from "firebase";
import 'firebase/firestore';
import 'firebase/auth'
import VueTextareaAutosize from 'vue-textarea-autosize'
import '@/assets/css/main.css'

Vue.use(VueTextareaAutosize)
// *******************************************************************************************
// *                       Configuration firebase                                            *
// *******************************************************************************************
Vue.config.productionTip = false
firebase.initializeApp({
  apiKey: "AIzaSyAspmXAsvIZLEjaXQXDtsn_8NnJBON-a4U",
    authDomain: "test-vuetify-calendar.firebaseapp.com",
    projectId: "test-vuetify-calendar",
    storageBucket: "test-vuetify-calendar.appspot.com",
    messagingSenderId: "335080616006",
    appId: "1:335080616006:web:a568db313a59ac78c8da47"

});
//  ******************************************************************************************
//  *               Gestion de l'authentification                                            *
//  * ****************************************************************************************
firebase.getCurrentUser = () => {
  return new Promise((resolve, reject) => {
      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
          unsubscribe();
          resolve(user);
      }, reject);
  })
};

export const db = firebase.firestore();
export const auth = firebase.auth();

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')


