// **************************************************************************************
// *   Configuration du serveur Express pour qu'il fonctionne avec Socket.io            *
// *                                                                                    *
// *  Express.js "écoute" la requête http et se charge d'envoyer en réponse une page    *
// *  qui contient le JavaScript pour établir une connexion WebSocket et gérer par      *
// *  la suite l'échange des messages ;                                                 *                                        *
// *                                                                                    *
// * Socket.io "écoute" et retransmet les messages entre les clients                    *
// * connectés et le serveur                                                            *
// **************************************************************************************

let app = require('express')();
// **************************************************************************************
// * inclure le module natif http qui nous permet par la suite de                        *
// * "relier" les serveurs Express et Socket.io dans une seule application               *
// **************************************************************************************
let http = require('http').Server(app);


let io = require('socket.io')(http);


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
});


http.listen(3000, () => {
    console.log('Listening on port *: 3000');
});


// *****************************************************************************************
// *                    ecoute de l'évènement de connexion:                                *
// *                                                                                       *
// * Socket.ON (): prend un nom d'événement et un rappel comme paramètres,                 *
// * il écoute un événement émis par le serveur ou vice versa,                             *
// * et le rappel est utilisé pour récupérer toutes les données associées à l'événement.   *
// *                                                                                       *
// * Socket.EMIT (): Cela émet / envoie un événement avec ou sans données aux sockets      *
// * d'écoute, y compris lui-même.                                                         *
// *                                                                                       *
// * Socket.Broadcast.Emit () : Cela émet un événement à d'autres clients connectés        *
// * sans lui-même inclus. Nous utiliserons ces méthodes pour envoyer différents événements*
// * à partir du serveur dans l'application de chat.                                       *
// ***************************************************************************************** 
  
// Le socket "écoute" les connexions qui seront établies à travers le fichier index.html servi par Express
io.on('connection', (socket) => {
    console.log(socket.client.conn.server.clientsCount)
// Un utilisateur se connecte
    socket.emit('connections', socket.client.conn.server.clientsCount);
// Un utilisateur se deconnecte
    socket.on('disconnect', () => {
        console.log("A user disconnected");
// Cette fonction émettra un message du serveur à tous les clients connectés, indiquant qu’un nouvel utilisateur s’est connecté
        socket.emit('connections', socket.client.conn.server.clientsCount);
    });
// Cette fonction sera exécutée lorsque le client enverra un événement 'message'
    socket.on('chat-message', (data) => {
        socket.broadcast.emit('chat-message', (data));
    });
// Cette fonction sera executé lorsque le client est entrain d'écrire
    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', (data));
    });

    socket.on('stopTyping', () => {
        socket.broadcast.emit('stopTyping');
    });
// Cette fonction sera executé quand un utilisateur se connecte
    socket.on('joined', (data) => {
        socket.broadcast.emit('joined', (data));
    });
// Cette fonction sera executé quand un utilisateur se déconnecte
    socket.on('leave', (data) => {
        socket.broadcast.emit('leave', (data));
    });

});